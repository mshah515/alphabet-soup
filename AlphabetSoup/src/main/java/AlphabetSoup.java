import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;

public class AlphabetSoup {

    /**
     * Checks if a given word can be found in a character grid in the specified direction starting from a given
     * row, column position.
     *
     * @param word         Word to search for.
     * @param grid         Character grid to search in.
     * @param currentRow   Row index to start searching from.
     * @param currentCol   Column index to start searching from.
     * @param direction    An integer array representing the direction of the search as [rowChange, colChange].
     *
     * @return  An ArrayList of ints specifying the end row and column indices if the word is found in the specified
     *          direction or an empty ArrayList if the word cannot be found in the grid for the given direction.
     */
    public static  ArrayList<Integer> checkDirection(String word, char[][] grid, int currentRow, int currentCol, int[] direction) {

        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 1; i < word.length(); i++) {
            currentRow += direction[0];
            currentCol += direction[1];

            if (currentRow < 0 || currentRow >= grid.length || currentCol < 0 || currentCol >= grid[0].length) {
                return result;
            }

            if (word.charAt(i) != grid[currentRow][currentCol]) {
                return result;
            }
        }
        result.add(currentRow);
        result.add(currentCol);

        return result;
    }

    /**
     * Searches for a word in a character grid. Walks through the grid to find the first letter, then searches in all
     * eight directions (defined as changes to the row and column) to find the whole word. If the word is found it prints
     * the start and end indices of the word in the grid.
     *
     * @param word      Word to search for.
     * @param grid      Character grid to search in.
     * @param gridRows  Number of rows in the grid.
     * @param gridCols  Number of columns in the grid.
     */
    public static void findWord(String word, char[][] grid, int gridRows, int gridCols) {

        // all the directions we can search in: up, down, left, right, up-right, up-left, down-right, down-left:
        int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}, {-1, 1}, {-1, -1}, {1, 1}, {1, -1}};

        for (int i = 0; i < gridRows; i++) {
            for (int j = 0; j < gridCols; j++) {

                // does current (row,column) match the first letter of the word we are searching for
                if (grid[i][j] == word.charAt(0)) {

                    // if the word is just one letter, that is the start and end
                    if (word.length() == 1) {
                        System.out.println(word + " " + i + ":" + j + " " + i + ":" + j);
                        return;
                    }

                    // first letter found, check in all eight directions
                    for (int[] direction : directions) {
                        ArrayList<Integer> end;
                        end = checkDirection(word, grid, i, j, direction);
                        if (end.size() == 2) {
                            System.out.println(word + " " + i + ":" + j + " " + end.get(0) + ":" + end.get(1));
                            return;
                        }
                    }
                }
            }
        }
    }


    public static void main(String[] args) throws FileNotFoundException {

        // read in the file, if it does not exist, throw an error:
        Scanner scan = new Scanner(new File(args[0]));


        // getting the dimensions of the grid
        String[] dimensions = scan.nextLine().split("x");
        int totalRows = Integer.parseInt(dimensions[0]);
        int totalCols = Integer.parseInt(dimensions[1]);


        // creating our puzzle grid
        char[][] grid = new char[totalRows][totalCols];
        // read in all the rows as a line
        for (int i = 0; i < totalRows; i++){
            String[] letters = scan.nextLine().split(" ");
            for (int j = 0; j < totalCols; j++) {
                grid[i][j] = letters[j].charAt(0);
            }
        }

        // remainder of file is all the words we are searching for, keep going as long as there is another line in the file
        while (scan.hasNextLine()) {
            String searchWord = scan.nextLine();

            // skip if a word is too long for our grid, it is invalid
            if(searchWord.length() > totalRows && searchWord.length() > totalCols) {
                continue;
            }

            // finding the given word in the word puzzle grid
            findWord(searchWord, grid, totalRows, totalCols);

        }
    }
}
