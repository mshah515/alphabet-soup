import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class CheckDirectionTest {

    static char[][] grid = {{'A', 'B', 'C'}, {'F', 'H', 'L'}, {'R', 'X', 'E'}};
    static String searchWord = "BHX";


    // correctIndex holds the [row, column] value of the end of the word we are searching for in the grid IF the word is valid
    // in the specified direction
    public void testCheckDirection(String word, char[][] grid, int currentRow, int currentCol, int[] direction, int[] correctIndex) {
        ArrayList<Integer> result = AlphabetSoup.checkDirection(word, grid, currentRow, currentCol, direction);
        ArrayList<Integer> expected = new ArrayList<>();
        try {
            expected.add(correctIndex[0]);
            expected.add(correctIndex[1]);
        } catch (Exception ignored) {}

        assertEquals(expected, result);
    }

    // test direction: down
    // start with correct location of  char B
    @Test
    public void checkDirectionDown() {
        testCheckDirection(searchWord, grid, 0, 1, new int[] {1, 0}, new int[] {2, 1});
    }

    // test direction: down-left
    // start with correct location of char B
    @Test
    public void checkDirectionDownLeft() {
        testCheckDirection(searchWord, grid, 0, 1, new int[] {1, -1}, new int[] {});
    }
}