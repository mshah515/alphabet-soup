import java.io.FileNotFoundException;
import java.util.List;
import java.util.Arrays;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class AlphabetSoupTest {

    // creating a TestFile class to define the file path and file output to test
    static class TestFile {
        String filePath;
        String expectedFileOutput;

        public TestFile(String file, String output) {
            this.filePath = file;
            this.expectedFileOutput = output;
        }
    }

    // a list of all the files we will test and their expected output
    static List<TestFile> allTestFiles() {
        return Arrays.asList(
                new TestFile("src/test/java/inputFiles/input1.txt", "ABC 0:0 0:2\nAEI 0:0 2:2"),
                new TestFile("src/test/java/inputFiles/input2.txt", "HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1"),
                new TestFile("src/test/java/inputFiles/input3.txt", "V 1:1 1:1\nGRC 1:0 3:2\nRVRX 0:1 3:1"),
                new TestFile("src/test/java/inputFiles/noSearchWords.txt", ""),
                new TestFile("src/test/java/inputFiles/oneWordTooLong.txt", "PGX 2:0 0:2\nPLMIJ 2:0 2:4"),
                new TestFile("src/test/java/inputFiles/backwardsSearching.txt", "DHR 3:2 1:0\nSHST 3:1 0:1\nYSU 2:0 0:2")
        );
    }


    @ParameterizedTest
    @MethodSource("allTestFiles")
    public void testAlphabetSoup(TestFile tf) throws FileNotFoundException {

        // Redirect System.out to capture the output
        ByteArrayOutputStream outputContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputContent));

        // Call the main method with the input file
        AlphabetSoup.main(new String[]{tf.filePath});

        // Restore the original System.out
        System.setOut(System.out);

        // Assert the output
        assertEquals(tf.expectedFileOutput, outputContent.toString().trim());
    }

}
